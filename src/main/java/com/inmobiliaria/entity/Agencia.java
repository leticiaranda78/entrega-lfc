package com.inmobiliaria.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "agencia")
public class Agencia {

	@Id
	@GeneratedValue
	@Column(name = "id_agencia")
	private int idAgencia;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "direccion")
	private String direccion;

	@Column(name = "telefono")
	private int telefono;

	@Column(name = "dias_horarios")
	private String diasHorarios;

	@Column(name = "id_estado")
	private int estado;

	public int getIdAgencia() {
		return idAgencia;
	}

	public void setIdAgencia(int idAgencia) {
		this.idAgencia = idAgencia;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public int getTelefono() {
		return telefono;
	}

	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}

	public String getDiasHorarios() {
		return diasHorarios;
	}

	public void setDiasHorarios(String diasHorarios) {
		this.diasHorarios = diasHorarios;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public Agencia(int idAgencia, String nombre, String direccion, int telefono, String diasHorarios, int estado) {
		super();
		this.idAgencia = idAgencia;
		this.nombre = nombre;
		this.direccion = direccion;
		this.telefono = telefono;
		this.diasHorarios = diasHorarios;
		this.estado = estado;
	}

	public Agencia() {

	}
}
