package com.inmobiliaria.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "inmueble")
public class Inmueble {

	@Id
	@GeneratedValue
	@Column(name = "id")
	private int id;

	@Column(name = "titulo")
	private String titulo;

	@Column(name = "descripcion")
	private String descripcion;

	@Column(name = "precio")
	private int precio;

	@Column(name = "direccion")
	private String direccion;

	@Column(name = "id_estado")
	private int estado;

	@Column(name = "foto1")
	private String foto1;

	@Column(name = "id_asesor")
	private int asesor;

	@Column(name = "ciudad")
	private String ciudad;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getPrecio() {
		return precio;
	}

	public void setPrecio(int precio) {
		this.precio = precio;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public String getFoto1() {
		return foto1;
	}

	public void setFoto1(String foto1) {
		this.foto1 = foto1;
	}

	public int getAsesor() {
		return asesor;
	}

	public void setAsesor(int asesor) {
		this.asesor = asesor;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public Inmueble(int id, String titulo, String descripcion, int precio, String direccion, int estado, String foto1,
			int asesor, String ciudad) {
		super();
		this.id = id;
		this.titulo = titulo;
		this.descripcion = descripcion;
		this.precio = precio;
		this.direccion = direccion;
		this.estado = estado;
		this.foto1 = foto1;
		this.asesor = asesor;
		this.ciudad = ciudad;
	}

	public Inmueble() {

	}

}
