package com.inmobiliaria.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "certificado")
public class Certificado {
	@Id
	@GeneratedValue
	@Column(name = "id_certificado")
	private int idCertificado;

	@Column(name = "titulo")
	private String titulo;

	@Column(name = "id_estado")
	private int estado;

	@Column(name = "descripcion")
	private String descripcion;

	public Certificado() {
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public Certificado(int idCertificado, String titulo, int estado, String descripcion) {
		this.idCertificado = idCertificado;
		this.titulo = titulo;
		this.estado = estado;
		this.descripcion = descripcion;
	}

	public int getIdCertificado() {
		return idCertificado;
	}

	public void setIdCertificado(int idCertificado) {
		this.idCertificado = idCertificado;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
