
package com.inmobiliaria.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ciudad")
public class Ciudad {
	@Id
	@GeneratedValue
	@Column(name = "id_ciudad")
	private int idCiudad;

	@Column(name = "descripcion")
	private String descripcion;

	@Column(name = "id_estado")
	private int estado;

	public Ciudad() {
	}

	public Ciudad(int idCiudad, String descripcion, int estado) {
		this.idCiudad = idCiudad;
		this.descripcion = descripcion;
		this.estado = estado;
	}

	public int getIdCiudad() {
		return idCiudad;
	}

	public void setIdCiudad(int idCiudad) {
		this.idCiudad = idCiudad;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

}
