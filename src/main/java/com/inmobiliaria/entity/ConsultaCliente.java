package com.inmobiliaria.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "consultacliente")
public class ConsultaCliente {

	@Id
	@GeneratedValue
	@Column(name = "id_consulta")
	private int idConsulta;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "apellido")
	private String apellido;

	@Column(name = "id_estado")
	private int estado;

	@Column(name = "email")
	private String email;

	@Column(name = "telefono")
	private String telefono;

	@Column(name = "consulta")
	private String consulta;

	public ConsultaCliente() {
	}

	public ConsultaCliente(int idConsulta, String nombre, int estado, String email, String telefono, String consulta,
			String apellido) {
		this.idConsulta = idConsulta;
		this.nombre = nombre;
		this.estado = estado;
		this.email = email;
		this.telefono = telefono;
		this.consulta = consulta;
		this.apellido = apellido;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public int getIdConsulta() {
		return idConsulta;
	}

	public void setIdConsulta(int idConsulta) {
		this.idConsulta = idConsulta;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getConsulta() {
		return consulta;
	}

	public void setConsulta(String consulta) {
		this.consulta = consulta;
	}

	@Override
	public String toString() {
		return "ConsultaCliente{" + "idConsulta=" + idConsulta + ", nombre=" + nombre + ", apellido=" + apellido
				+ ", estado=" + estado + ", email=" + email + ", telefono=" + telefono + ", consulta=" + consulta + '}';
	}

}