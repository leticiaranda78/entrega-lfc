
package com.inmobiliaria.model;

public class CiudadModel {
	private int idCiudad;
	private String descripcion;
	private int estado;

	public CiudadModel() {
	}

	public CiudadModel(int idCiudad, String descripcion, int estado) {
		this.idCiudad = idCiudad;
		this.descripcion = descripcion;
		this.estado = estado;
	}

	public int getIdCiudad() {
		return idCiudad;
	}

	public void setIdCiudad(int idCiudad) {
		this.idCiudad = idCiudad;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	@Override
	public String toString() {
		return "CiudadModel[" + "idCiudad=" + idCiudad + ", descripcion=" + descripcion + ']';
	}

}
