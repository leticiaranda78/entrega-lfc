package com.inmobiliaria.model;

public class AsesorModel {

	private int idAsesor;
	private int nroCedula;
	private String nombre;
	private String apellido;
	private String fechaNacimiento;
	private String email;
	private String direccion;
	private int estado;

	public int getIdAsesor() {
		return idAsesor;
	}

	public void setIdAsesor(int idAsesor) {
		this.idAsesor = idAsesor;
	}

	public int getNroCedula() {
		return nroCedula;
	}

	public void setNroCedula(int nroCedula) {
		this.nroCedula = nroCedula;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public AsesorModel(int idAsesor, int nroCedula, String nombre, String apellido, String fechaNacimiento,
			String email, String direccion, int estado) {
		super();
		this.idAsesor = idAsesor;
		this.nroCedula = nroCedula;
		this.nombre = nombre;
		this.apellido = apellido;
		this.fechaNacimiento = fechaNacimiento;
		this.email = email;
		this.direccion = direccion;
		this.estado = estado;

	}

	public AsesorModel() {

	}

	@Override
	public String toString() {
		return "AsesorModel [idAsesor=" + idAsesor + ", nroCedula=" + nroCedula + ", nombre=" + nombre + ", apellido="
				+ apellido + ", fechaNacimiento=" + fechaNacimiento + ", email=" + email + ", direccion=" + direccion
				+ ", estado=" + estado + "]";
	}

}
