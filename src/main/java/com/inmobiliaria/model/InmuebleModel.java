package com.inmobiliaria.model;

public class InmuebleModel {

	private int id;
	private String titulo;
	private String descripcion;
	private int precio;
	private String direccion;
	private int estado;
	private String foto1;
	private int asesor;
	private String ciudad;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getPrecio() {
		return precio;
	}

	public void setPrecio(int precio) {
		this.precio = precio;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public String getFoto1() {
		return foto1;
	}

	public void setFoto1(String foto1) {
		this.foto1 = foto1;
	}

	public int getAsesor() {
		return asesor;
	}

	public void setAsesor(int asesor) {
		this.asesor = asesor;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public InmuebleModel(int id, String titulo, String descripcion, int precio, String direccion, int estado,
			String foto1, int asesor, String ciudad) {
		super();
		this.id = id;
		this.titulo = titulo;
		this.descripcion = descripcion;
		this.precio = precio;
		this.direccion = direccion;
		this.estado = estado;
		this.foto1 = foto1;
		this.asesor = asesor;
		this.ciudad = ciudad;
	}

	public InmuebleModel() {

	}

	@Override
	public String toString() {
		return "InmuebleModel [id=" + id + ", titulo=" + titulo + ", descripcion=" + descripcion + ", precio=" + precio
				+ ", direccion=" + direccion + ", estado=" + estado + ", foto1=" + foto1 + ", asesor=" + asesor
				+ ", ciudad=" + ciudad + "]";
	}

}
