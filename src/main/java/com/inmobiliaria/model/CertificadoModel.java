package com.inmobiliaria.model;

public class CertificadoModel {
	
	private int idCertificado;
	private String titulo;
	private int estado;
	private String descripcion;

	public CertificadoModel() {
	}

	public CertificadoModel(int idCertificado, String titulo, int estado, String descripcion) {
		this.idCertificado = idCertificado;
		this.titulo = titulo;
		this.estado = estado;
		this.descripcion = descripcion;
	}

	public int getIdCertificado() {
		return idCertificado;
	}

	public void setIdCertificado(int idCertificado) {
		this.idCertificado = idCertificado;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public String toString() {
		return "CertificadoModel{" + "idCertificado=" + idCertificado + ", titulo=" + titulo + ", estado=" + estado
				+ ", descripcion=" + descripcion + '}';
	}

}
