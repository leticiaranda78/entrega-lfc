package com.inmobiliaria.model;

public class ConsultaClienteModel {

	private int idConsulta;
	private String nombre;
	private String apellido;
	private int estado;
	private String email;
	private String telefono;
	private String consulta;

	public ConsultaClienteModel() {
	}

	public ConsultaClienteModel(int idConsulta, String nombre, int estado, String email, String telefono,
			String consulta, String apellido) {
		this.idConsulta = idConsulta;
		this.nombre = nombre;
		this.apellido = apellido;
		this.estado = estado;
		this.email = email;
		this.telefono = telefono;
		this.consulta = consulta;

	}

	public int getIdConsulta() {
		return idConsulta;
	}

	public void setIdConsulta(int idConsulta) {
		this.idConsulta = idConsulta;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getConsulta() {
		return consulta;
	}

	public void setConsulta(String consulta) {
		this.consulta = consulta;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	@Override
	public String toString() {
		return "ConsultaClienteModel{" + "idConsulta=" + idConsulta + ", nombre=" + nombre + ", apellido=" + apellido
				+ ", estado=" + estado + ", email=" + email + ", telefono=" + telefono + ", consulta=" + consulta + '}';
	}

}
