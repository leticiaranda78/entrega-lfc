package com.inmobiliaria.model;

public class AgenciaModel {
	private int idAgencia;
	private String nombre;
	private String direccion;
	private int telefono;
	private String diasHorarios;
	private int estado;

	public int getIdAgencia() {
		return idAgencia;
	}

	public void setIdAgencia(int idAgencia) {
		this.idAgencia = idAgencia;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public int getTelefono() {
		return telefono;
	}

	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}

	public String getDiasHorarios() {
		return diasHorarios;
	}

	public void setDiasHorarios(String diasHorarios) {
		this.diasHorarios = diasHorarios;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public AgenciaModel(int idAgencia, String nombre, String direccion, int telefono, String diasHorarios, int estado) {
		super();
		this.idAgencia = idAgencia;
		this.nombre = nombre;
		this.direccion = direccion;
		this.telefono = telefono;
		this.diasHorarios = diasHorarios;
		this.estado = estado;
	}

	public AgenciaModel() {

	}

	@Override
	public String toString() {
		return "AgenciaModel [idAgencia=" + idAgencia + ", nombre=" + nombre + ", direccion=" + direccion
				+ ", telefono=" + telefono + ", diasHorarios=" + diasHorarios + ", estado=" + estado + "]";
	}

}
