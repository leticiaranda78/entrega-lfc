package com.inmobiliaria.repository;

import com.inmobiliaria.entity.Certificado;
import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("certificadoRepository")
public interface CertificadoRepository extends JpaRepository<Certificado, Serializable> {

	// public abstract Certificado findByCertId(int id);
	// List<Certificado> findByTituloCertificado(String titulo);
}
