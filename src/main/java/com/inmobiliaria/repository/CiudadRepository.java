
package com.inmobiliaria.repository;

import com.inmobiliaria.entity.Ciudad;
import java.io.Serializable;
//import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CiudadRepository extends JpaRepository<Ciudad, Serializable> {
   // List<Ciudad> findByCiudadDescripcion(String descripcion);
}
