package com.inmobiliaria.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.inmobiliaria.entity.Asesor;

@Repository("asesorRepository")
public interface AsesorRespository extends JpaRepository<Asesor, Serializable> {
	
	List<Asesor> findByNroCedula(int nroCedula);

}
