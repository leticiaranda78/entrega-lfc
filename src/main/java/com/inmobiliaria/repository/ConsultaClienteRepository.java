package com.inmobiliaria.repository;

import com.inmobiliaria.entity.ConsultaCliente;
import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("consultaclienteRepository")
public interface ConsultaClienteRepository extends JpaRepository<ConsultaCliente, Serializable> {

}
