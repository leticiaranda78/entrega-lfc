package com.inmobiliaria.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.inmobiliaria.entity.Agencia;

@Repository("agenciaRepository")
public interface AgenciaRespository extends JpaRepository<Agencia, Serializable> {

	List<Agencia> findByNombre(String nombre);

}
