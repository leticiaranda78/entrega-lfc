package com.inmobiliaria.constant;

public class ViewConstant {

	// para no cambiar las vistas uno por uno
	public static final String AGREGAR_FORM = "agregar";
	public static final String ADMINISTRAR = "administrar";
	public static final String INDEX = "index";
	public static final String LOGIN = "login";
	public static final String ERROR500 = "error/500";
	public static final String ASESOR = "asesores";
	public static final String ASESOR_FORM = "asesorForm";
	public static final String BUSQUEDA_FORM = "busqueda";
	public static final String AGENCIA = "agencias";
	public static final String AGENCIA_FORM = "agenciaForm";
	public static final String BUSQUEDAGENCIA_FORM = "busquedaAgencia";
	public static final String CIUDAD = "ciudades";
	public static final String CIUDAD_FORM = "ciudadForm";
	public static final String BUSQUEDAINMUEBLE_FORM = "busquedaInmueble";
	
	public static final String CERTIFICADO = "certificados";
	public static final String CERTIFICADO_FORM = "certificadoForm";
	public static final String BUSQUEDACERTIFICADO_FORM = "busquedacertificado";
	public static final String CCLIENTE_FORM = "consultaForm";
	public static final String CONSULTA = "consultas";
}
