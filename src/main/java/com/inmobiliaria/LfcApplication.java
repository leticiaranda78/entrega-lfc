package com.inmobiliaria;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LfcApplication {

	public static void main(String[] args) {
		SpringApplication.run(LfcApplication.class, args);
	}

}
