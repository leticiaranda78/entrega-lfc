package com.inmobiliaria.controller;

import com.inmobiliaria.constant.ViewConstant;
import com.inmobiliaria.model.CiudadModel;
import com.inmobiliaria.service.CiudadService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/ciudad")
public class CiudadController {

	private static final Log LOG = LogFactory.getLog(AsesorController.class);

	@Autowired
	@Qualifier("ciudadServiceImpl")
	private CiudadService ciudadService;

	@GetMapping("/cancel")
	public String cancelCiudad() {
		return "redirect:/ciudad/showCiudades";
	}

	@GetMapping("/agregar")
	private String redirectAgregarForm(@RequestParam(name = "idCiudad", required = false) int idCiudad, Model model) {
		CiudadModel ciudadModel = new CiudadModel();
		if (idCiudad != 0) {
			ciudadModel = ciudadService.findCiudadByIdModel(idCiudad);
		}
		model.addAttribute("ciudadModel", ciudadModel);
		return ViewConstant.CIUDAD_FORM;// ver VER
	}

	@PostMapping("/addciudad")
	public String addCiudad(CiudadModel ciudadModel, Model model) {
		LOG.info("METHOD: addCiudad() -- PARAMS:    " + ciudadModel.toString());
		if (null != ciudadService.addCiudad(ciudadModel)) {
			model.addAttribute("result", 1);
		} else {
			model.addAttribute("result", 0);
		}
		return "redirect:/ciudad/showCiudades";
	}

	@GetMapping("/showCiudades")
	public ModelAndView showCiudades() {
		ModelAndView mav = new ModelAndView(ViewConstant.CIUDAD);
		mav.addObject("ciudades", ciudadService.listAllCiudad());
		return mav;
	}

	@GetMapping("removeCiudad")
	public ModelAndView removeCiudad(@RequestParam(name = "idCiudad", required = true) int idCiudad, Model model) {
		CiudadModel ciudModel = new CiudadModel();
		ciudModel = ciudadService.removeCiudad(idCiudad);

		if (ciudModel.getIdCiudad() == 0) {
			model.addAttribute("resultado", 0);
		} else {
			model.addAttribute("resultado", 1);
		}
		return showCiudades();
	}
}
