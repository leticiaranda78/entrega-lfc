package com.inmobiliaria.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.inmobiliaria.constant.ViewConstant;
import com.inmobiliaria.model.AgenciaModel;
import com.inmobiliaria.service.AgenciaService;

@Controller
@RequestMapping("/agencia")
public class AgenciaController {
	
	private static final Log LOG = LogFactory.getLog(AgenciaController.class);

	@Autowired
	@Qualifier("agenciaServiceImpl")
	private AgenciaService agenciaService;

	// método que cancela la operación
	@GetMapping("/cancel")
	public String cancel() {
		return "redirect:/agencia/showAgencias";
	}

	// método que lleva a la ventana de agregar
	@GetMapping("/agregar")
	private String redirectAgenciaForm(@RequestParam(name = "idAgencia", required = false) int idAgencia, Model model) {
		AgenciaModel agenciaModel = new AgenciaModel();
		if (idAgencia != 0) {
			agenciaModel = agenciaService.findAgenciaByIdModel(idAgencia);
		}
		model.addAttribute("agenciaModel", agenciaModel);
		return ViewConstant.AGENCIA_FORM;
	}

	@PostMapping("/addagencia")
	public String addAgencia(AgenciaModel agenciaModel, Model model) {
		LOG.info("METHOD: addAgencia() -- PARAMS:  " + agenciaModel.toString());

		if (null != agenciaService.addAgencia(agenciaModel)) {
			model.addAttribute("result", 1);
		} else {
			model.addAttribute("result", 0);
		}

		return "redirect:/agencia/showAgencias";
	}

	@GetMapping("/showAgencias")
	public ModelAndView showAgencias() {
		ModelAndView mav = new ModelAndView(ViewConstant.AGENCIA);
		mav.addObject("agencias", agenciaService.listAllAgencia());
		return mav;
	}

	@GetMapping("removeAgencia")
	public ModelAndView removeAgencia(@RequestParam(name = "idAgencia", required = true) int idAgencia, Model model) {

		AgenciaModel aModel = new AgenciaModel();

		aModel = agenciaService.removeAgencia(idAgencia);

		if (aModel.getIdAgencia() == 0) {
			model.addAttribute("resultado", 0);
		} else {
			model.addAttribute("resultado", 1);
		}

		return showAgencias();
	}
	
}
