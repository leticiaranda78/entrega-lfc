package com.inmobiliaria.controller;

import com.inmobiliaria.constant.ViewConstant;

import com.inmobiliaria.model.ConsultaClienteModel;

import com.inmobiliaria.service.ConsultaClienteService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/consultacliente")
public class ConsultaClienteController {

	private static final Log LOG = LogFactory.getLog(ConsultaClienteController.class);

	@Autowired
	@Qualifier("consultaclienteServiceImpl")
	private ConsultaClienteService ccService;

	@GetMapping("/cancel")
	public String cancel() {
		return "redirect:/consultacliente/showConsultas";
	}

	@GetMapping("/agregar")
	private String redirectAgregarForm(@RequestParam(name = "idConsulta", required = false) int idConsulta,
			Model model) {
		ConsultaClienteModel ccModel = new ConsultaClienteModel();
		if (idConsulta != 0) {
			ccModel = ccService.findConsultaClienteByIdModel(idConsulta);
		}
		model.addAttribute("ccModel", ccModel);
		return ViewConstant.CCLIENTE_FORM;
	}

	@PostMapping("/addconsulta")
	public String addConsultaCliente(ConsultaClienteModel ccModel, Model model) {
		LOG.info("METHOD: addConsultaCliente() -- PARAMS:  " + ccModel.toString());

		if (null != ccService.addConsultaCliente(ccModel)) {
			model.addAttribute("result", 1);
		} else {
			model.addAttribute("result", 0);
		}

		return "redirect:/consultacliente/showConsultas";
	}

	@GetMapping("/showConsultas")
	public ModelAndView showConsultas() {
		ModelAndView mav = new ModelAndView(ViewConstant.CONSULTA);
		mav.addObject("consultas", ccService.listAllConsultaCliente());
		System.out.println(mav);
		return mav;
	}

	@GetMapping("removeConsulta")
	public ModelAndView removeConsulta(@RequestParam(name = "idConsulta", required = true) int idConsulta,
			Model model) {

		ConsultaClienteModel ccModel = new ConsultaClienteModel();

		ccModel = ccService.removeConsultaCliente(idConsulta);

		if (ccModel.getIdConsulta() == 0) {
			model.addAttribute("resultado", 0);
		} else {
			model.addAttribute("resultado", 1);
		}

		return showConsultas();
	}

}
