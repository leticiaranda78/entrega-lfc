package com.inmobiliaria.controller;

import com.inmobiliaria.constant.ViewConstant;
import com.inmobiliaria.model.CertificadoModel;
import com.inmobiliaria.service.CertificadoService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/certificado")
public class CertificadoController {
	private static final Log LOG = LogFactory.getLog(AsesorController.class);

	@Autowired
	@Qualifier("certificadoServiceImpl")
	private CertificadoService certificadoService;

	@GetMapping("/cancel")
	public String cancelCertificado() {
		return "redirect:/certificado/showCertificados";
	}

	@GetMapping("/agregar")
	private String redirectAgregarForm(@RequestParam(name = "idCertificado", required = false) int idCertificado,
			Model model) {
		CertificadoModel certificadoModel = new CertificadoModel();
		if (idCertificado != 0) {
			certificadoModel = certificadoService.findCertificadoByIdModel(idCertificado);
		}
		model.addAttribute("certificadoModel", certificadoModel);
		return ViewConstant.CERTIFICADO_FORM;
	}

	@PostMapping("/addcertificado")
	public String addCertificado(CertificadoModel certificadoModel, Model model) {
		LOG.info("METHOD: addCiudad() -- PARAMS:      " + certificadoModel.toString());
		if (null != certificadoService.addCertificado(certificadoModel)) {
			model.addAttribute("result", 1);
		} else {
			model.addAttribute("result", 0);
		}
		return "redirect:/certificado/showCertificados";
	}

	@GetMapping("/showCertificados")
	public ModelAndView showCertificados() {
		ModelAndView mav = new ModelAndView(ViewConstant.CERTIFICADO);
		mav.addObject("certificados", certificadoService.listAllCertificado());
		return mav;
	}

	@GetMapping("removeCertificado")
	public ModelAndView removeCertificado(@RequestParam(name = "idCertificado", required = true) int idCertificado,
			Model model) {
		CertificadoModel certificadoModel = new CertificadoModel();
		certificadoModel = certificadoService.removeCertificado(idCertificado);

		if (certificadoModel.getIdCertificado() == 0) {
			model.addAttribute("resultado", 0);

		} else {
			model.addAttribute("resultado", 1);
		}
		return showCertificados();
	}
}
