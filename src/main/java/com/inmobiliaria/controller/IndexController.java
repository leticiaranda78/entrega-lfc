package com.inmobiliaria.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.inmobiliaria.constant.ViewConstant;
import com.inmobiliaria.entity.Inmueble;
import com.inmobiliaria.repository.InmuebleRepository;
import com.inmobiliaria.service.InmuebleService;
import com.inmobiliaria.utils.RenderizadorPaginas;

@Controller
public class IndexController {
	
	@Autowired
	@Qualifier("inmuebleServiceImpl")
	private InmuebleService inmuebleService;
	
	@Autowired
	@Qualifier("inmuebleRepository")
	private InmuebleRepository inmuebleRepository;
	
	@GetMapping("/administrar")
	public String redirectAgregarForm() {
		return ViewConstant.ADMINISTRAR;
	}
	
	@GetMapping("/index")
	public String redirectIndex(@RequestParam(name = "page", defaultValue = "0") int page, Model model) {
		Pageable inmueblePageable = PageRequest.of(page, 1);
		Page<Inmueble> inmueble = inmuebleRepository.findAll(inmueblePageable);
		RenderizadorPaginas<Inmueble> renderizadorPaginas = new RenderizadorPaginas<Inmueble>("/index", inmueble);
		model.addAttribute("page", renderizadorPaginas);
		model.addAttribute("inmuebles", inmueble);
		
		return ViewConstant.INDEX;
	}
	
}
