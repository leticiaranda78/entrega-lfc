package com.inmobiliaria.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.inmobiliaria.constant.ViewConstant;
import com.inmobiliaria.model.AgenciaModel;
import com.inmobiliaria.service.AgenciaService;
import com.inmobiliaria.service.BusquedaAgenciaService;

@Controller
@RequestMapping("/busquedaAgencia")
public class BusquedaAController {

	@Autowired
	@Qualifier("busquedaAgenciaServiceImpl")
	private BusquedaAgenciaService busquedaAService;

	@Autowired
	@Qualifier("agenciaServiceImpl")
	private AgenciaService agenciaService;

	@GetMapping("/busquedaAForm")
	public String busquedaAForm(Model model) {
		model.addAttribute("agencia", new AgenciaModel());
		return ViewConstant.BUSQUEDAGENCIA_FORM;
	}

	@GetMapping("/buscarAgencia")
	public String buscarPorNombre(@RequestParam String nombre, Model model,
			@ModelAttribute("agencia") AgenciaModel agenciaModel) {
		List<AgenciaModel> listaAgencias = busquedaAService.buscarPorNombre(nombre);

		listaAgencias.stream().filter(p -> p.getNombre().equals(nombre));

		if (!listaAgencias.isEmpty()) {
			model.addAttribute("agenciaPorNombre", busquedaAService.buscarPorNombre(nombre));
		} else {
			model.addAttribute("result", 1);
		}

		if (nombre == "") {
			model.addAttribute("result", 0);
		}

		return ViewConstant.BUSQUEDAGENCIA_FORM;
	}

}
