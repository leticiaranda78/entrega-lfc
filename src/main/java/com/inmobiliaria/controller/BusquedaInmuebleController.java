package com.inmobiliaria.controller;

import com.inmobiliaria.constant.ViewConstant;
import com.inmobiliaria.model.InmuebleModel;
import com.inmobiliaria.service.BusquedaInmuebleService;

import com.inmobiliaria.service.InmuebleService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/busquedaI")
public class BusquedaInmuebleController {

	@Autowired
	@Qualifier("busquedaInmuebleServiceImpl")
	private BusquedaInmuebleService busquedaIService;

	@Autowired
	@Qualifier("inmuebleServiceImpl")
	private InmuebleService inmuebleService;

	@GetMapping("/busquedaIForm")
	public String busquedaIForm(Model model) {
		model.addAttribute("inmueble", new InmuebleModel());
		return ViewConstant.BUSQUEDAINMUEBLE_FORM;
	}

	@GetMapping("/buscarI")
	public String buscarPorTitulo(@RequestParam String titulo, Model model,
			@ModelAttribute("inmueble") InmuebleModel inmuebleModel) {
		List<InmuebleModel> iModel = busquedaIService.buscarPorTitulo(titulo);

		iModel.stream().filter(p -> p.getTitulo() == titulo);

		if (!iModel.isEmpty()) {
			model.addAttribute("inmueblePorTitulo", busquedaIService.buscarPorTitulo(titulo));
		} else {
			model.addAttribute("result", 1);
		}

		if (titulo == "") {
			model.addAttribute("result", 0);
		}

		return ViewConstant.BUSQUEDAINMUEBLE_FORM;
	}
}
