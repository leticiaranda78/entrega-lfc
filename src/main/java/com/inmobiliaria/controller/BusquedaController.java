package com.inmobiliaria.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.inmobiliaria.constant.ViewConstant;
import com.inmobiliaria.model.AsesorModel;
import com.inmobiliaria.service.AsesorService;
import com.inmobiliaria.service.BusquedaService;

@Controller
@RequestMapping("/busqueda")
public class BusquedaController {

	@Autowired
	@Qualifier("busquedaServiceImpl")
	private BusquedaService busquedaService;

	@Autowired
	@Qualifier("asesorServiceImpl")
	private AsesorService asesorService;

	@GetMapping("/busquedaForm")
	public String busquedaForm(Model model) {
		model.addAttribute("asesor", new AsesorModel());
		return ViewConstant.BUSQUEDA_FORM;
	}

	@GetMapping("/buscar")
	public String buscarPorNroCedula(@RequestParam int nroCedula, Model model,
			@ModelAttribute("asesor") AsesorModel asesorModel) {
		List<AsesorModel> aModel = busquedaService.buscarPorNroCedula(nroCedula);

		aModel.stream().filter(p -> p.getNroCedula() == nroCedula);

		if (!aModel.isEmpty()) {
			model.addAttribute("asesorPorNombre", busquedaService.buscarPorNroCedula(nroCedula));
		} else {
			model.addAttribute("result", 1);
		}

		if (nroCedula == 0) {
			model.addAttribute("result", 0);
		}

		return ViewConstant.BUSQUEDA_FORM;
	}

}
