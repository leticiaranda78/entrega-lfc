package com.inmobiliaria.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.inmobiliaria.constant.ViewConstant;
import com.inmobiliaria.model.AsesorModel;
import com.inmobiliaria.service.AsesorService;

@Controller
@RequestMapping("/asesor")
public class AsesorController {

	private static final Log LOG = LogFactory.getLog(AsesorController.class);

	@Autowired
	@Qualifier("asesorServiceImpl")
	private AsesorService asesorService;

	// método que cancela la operación
	@GetMapping("/cancel")
	public String cancel() {
		return "redirect:/asesor/showAsesores";
	}

	// método que lleva a la ventana de agregar
	@GetMapping("/agregar")
	private String redirectAgregarForm(@RequestParam(name = "idAsesor", required = false) int idAsesor, Model model) {
		AsesorModel asesorModel = new AsesorModel();
		if (idAsesor != 0) {
			asesorModel = asesorService.findAsesorByIdModel(idAsesor);
		}
		model.addAttribute("asesorModel", asesorModel);
		return ViewConstant.ASESOR_FORM;
	}

	@PostMapping("/addasesor")
	public String addAsesor(AsesorModel asesorModel, Model model) {
		LOG.info("METHOD: addAsesor() -- PARAMS:  " + asesorModel.toString());

		if (null != asesorService.addAsesor(asesorModel)) {
			model.addAttribute("result", 1);
		} else {
			model.addAttribute("result", 0);
		}

		return "redirect:/asesor/showAsesores";
	}

	@GetMapping("/showAsesores")
	public ModelAndView showAsesores() {
		ModelAndView mav = new ModelAndView(ViewConstant.ASESOR);
		mav.addObject("asesores", asesorService.listAllAsesor());
		return mav;
	}

	@GetMapping("removeAsesor")
	public ModelAndView removeInmueble(@RequestParam(name = "idAsesor", required = true) int idAsesor, Model model) {

		AsesorModel asModel = new AsesorModel();

		asModel = asesorService.removeAsesor(idAsesor);

		if (asModel.getIdAsesor() == 0) {
			model.addAttribute("resultado", 0);
		} else {
			model.addAttribute("resultado", 1);
		}

		return showAsesores();
	}

}
