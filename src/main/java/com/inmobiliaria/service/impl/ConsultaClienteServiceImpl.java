package com.inmobiliaria.service.impl;

import com.inmobiliaria.component.ConsultaClienteConverter;

import com.inmobiliaria.entity.ConsultaCliente;

import com.inmobiliaria.model.ConsultaClienteModel;
import com.inmobiliaria.repository.ConsultaClienteRepository;
import com.inmobiliaria.service.ConsultaClienteService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("consultaclienteServiceImpl")
public class ConsultaClienteServiceImpl implements ConsultaClienteService {

	@Autowired
	@Qualifier("consultaclienteRepository")
	private ConsultaClienteRepository ccRepository;

	@Autowired
	@Qualifier("consultaclienteConverter")
	private ConsultaClienteConverter ccConverter;

	@Override
	public ConsultaClienteModel addConsultaCliente(ConsultaClienteModel ccModel) {
		ccModel.setEstado(1);
		ConsultaCliente cc = ccRepository.save(ccConverter.convertConsultaClienteModel2ConsultaCliente(ccModel));
		return ccConverter.convertConsultaCliente2ConsultaClienteModel(cc);
	}

	@Override
	public List<ConsultaClienteModel> listAllConsultaCliente() {
		List<ConsultaCliente> cc = ccRepository.findAll();
		List<ConsultaClienteModel> ccModel = new ArrayList<ConsultaClienteModel>();
		for (ConsultaCliente consultas : cc) {
			ccModel.add(ccConverter.convertConsultaCliente2ConsultaClienteModel(consultas));

		}
		return ccModel;
	}

	@Override
	public ConsultaCliente findConsultaClienteById(int idConsulta) {
		ConsultaCliente cc = new ConsultaCliente(0, "", 0, "", "", "", "");
		ConsultaCliente consulta = ccRepository.findById(idConsulta).orElse(null);
		if (null != consulta) {
			return consulta;
		}
		return cc;
	}

	@Override
	public ConsultaClienteModel removeConsultaCliente(int idConsultaCliente) {
		ConsultaClienteModel ccModel = new ConsultaClienteModel();
		ccModel = ccConverter.convertConsultaCliente2ConsultaClienteModel(findConsultaClienteById(idConsultaCliente));
		ccModel.setEstado(2);
		ccRepository.save(ccConverter.convertConsultaClienteModel2ConsultaCliente(ccModel));
		return ccModel;
	}

	@Override
	public ConsultaClienteModel findConsultaClienteByIdModel(int idConsultaCliente) {
		ConsultaClienteModel ccModel = new ConsultaClienteModel();
		ccModel = ccConverter.convertConsultaCliente2ConsultaClienteModel(findConsultaClienteById(idConsultaCliente));
		return ccModel;
	}

}
