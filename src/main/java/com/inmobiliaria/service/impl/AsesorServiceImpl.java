package com.inmobiliaria.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.inmobiliaria.component.AsesorConverter;
import com.inmobiliaria.entity.Asesor;
import com.inmobiliaria.model.AsesorModel;
import com.inmobiliaria.repository.AsesorRespository;
import com.inmobiliaria.service.AsesorService;

@Service("asesorServiceImpl")
public class AsesorServiceImpl implements AsesorService {

	@Autowired
	@Qualifier("asesorRepository")
	private AsesorRespository asesorRepository;

	@Autowired
	@Qualifier("asesorConverter")
	private AsesorConverter asesorConverter;

	@Override
	public AsesorModel addAsesor(AsesorModel asesorModel) {
		asesorModel.setEstado(1);
		Asesor asesor = asesorRepository.save(asesorConverter.convertAsesorModel2Asesor(asesorModel));
		return asesorConverter.convertAsesor2AsesorModel(asesor);
	}

	@Override
	public List<AsesorModel> listAllAsesor() {
		List<Asesor> asesores = asesorRepository.findAll();
		List<AsesorModel> asesorModel = new ArrayList<AsesorModel>();
		for (Asesor asesor : asesores) {
			asesorModel.add(asesorConverter.convertAsesor2AsesorModel(asesor));
		}
		return asesorModel;
	}

	@Override
	public Asesor findAsesorById(int idAsesor) {
		Asesor as = new Asesor(0, 0, "", "", "", "", "", 2);
		Asesor asesor = asesorRepository.findById(idAsesor).orElse(null);
		if (null != asesor) {
			return asesor;
		}
		return as;
	}

	@Override
	public AsesorModel removeAsesor(int idAsesor) {
		AsesorModel asModel = new AsesorModel();
		asModel = asesorConverter.convertAsesor2AsesorModel(findAsesorById(idAsesor));
		asModel.setEstado(2);
		asesorRepository.save(asesorConverter.convertAsesorModel2Asesor(asModel));
		return asModel;
	}

	@Override
	public AsesorModel findAsesorByIdModel(int idAsesor) {
		AsesorModel asesorModel = new AsesorModel();
		asesorModel = asesorConverter.convertAsesor2AsesorModel(findAsesorById(idAsesor));
		return asesorModel;
	}

}
