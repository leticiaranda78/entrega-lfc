package com.inmobiliaria.service.impl;

import com.inmobiliaria.component.InmuebleConverter;
import com.inmobiliaria.entity.Inmueble;
import com.inmobiliaria.model.InmuebleModel;
import com.inmobiliaria.repository.InmuebleRepository;
import com.inmobiliaria.service.BusquedaInmuebleService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("busquedaInmuebleServiceImpl")
public class BusquedaInmuebleServiceImpl implements BusquedaInmuebleService {

	@Autowired
	@Qualifier("inmuebleRepository")
	private InmuebleRepository inmuebleRepository;

	@Autowired
	@Qualifier("inmuebleConverter")
	private InmuebleConverter inmuebleConverter;

	@Override
	public List<InmuebleModel> buscarPorTitulo(String titulo) {
		List<Inmueble> inmuebleList = inmuebleRepository.findByTitulo(titulo);
		List<InmuebleModel> inmuebleModel = new ArrayList<InmuebleModel>();
		for (Inmueble inmueble : inmuebleList) {
			inmuebleModel.add(inmuebleConverter.convertInmuebleToContactModel(inmueble));
		}
		return inmuebleModel;
	}

}
