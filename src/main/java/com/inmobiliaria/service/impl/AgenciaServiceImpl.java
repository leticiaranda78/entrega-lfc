package com.inmobiliaria.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.inmobiliaria.component.AgenciaConverter;
import com.inmobiliaria.entity.Agencia;
import com.inmobiliaria.model.AgenciaModel;
import com.inmobiliaria.repository.AgenciaRespository;
import com.inmobiliaria.service.AgenciaService;

@Service("agenciaServiceImpl")
public class AgenciaServiceImpl implements AgenciaService{
	
	@Autowired
	@Qualifier("agenciaRepository")
	private AgenciaRespository agenciaRepository;

	@Autowired
	@Qualifier("agenciaConverter")
	private AgenciaConverter agenciaConverter;

	@Override
	public AgenciaModel addAgencia(AgenciaModel agenciaModel) {
		agenciaModel.setEstado(1);
		Agencia agencia = agenciaRepository.save(agenciaConverter.convertAgenciaModel2Agencia(agenciaModel));
		return agenciaConverter.convertAgencia2AgenciaModel(agencia);
	}

	@Override
	public List<AgenciaModel> listAllAgencia() {
		List<Agencia> agencias = agenciaRepository.findAll();
		List<AgenciaModel> agenciaModel = new ArrayList<AgenciaModel>();
		for (Agencia agencia : agencias) {
			agenciaModel.add(agenciaConverter.convertAgencia2AgenciaModel(agencia));
		}
		return agenciaModel;
	}

	@Override
	public Agencia findAgenciaById(int idAgencia) {
		Agencia a = new Agencia(0, "", "", 0, "", 2);
		Agencia agencia = agenciaRepository.findById(idAgencia).orElse(null);
		if (null != agencia) {
			return agencia;
		}
		return a;
	}

	@Override
	public AgenciaModel removeAgencia(int idAgencia) {
		AgenciaModel aModel = new AgenciaModel();
		aModel = agenciaConverter.convertAgencia2AgenciaModel(findAgenciaById(idAgencia));
		aModel.setEstado(2);
		agenciaRepository.save(agenciaConverter.convertAgenciaModel2Agencia(aModel));
		return aModel;
	}

	@Override
	public AgenciaModel findAgenciaByIdModel(int idAgencia) {
		AgenciaModel agenciaModel = new AgenciaModel();
		agenciaModel = agenciaConverter.convertAgencia2AgenciaModel(findAgenciaById(idAgencia));
		return agenciaModel;
	}
}
