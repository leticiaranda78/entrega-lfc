package com.inmobiliaria.service.impl;

import com.inmobiliaria.component.CertificadoConverter;
import com.inmobiliaria.entity.Certificado;
import com.inmobiliaria.model.CertificadoModel;
import com.inmobiliaria.repository.CertificadoRepository;
import com.inmobiliaria.service.CertificadoService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("certificadoServiceImpl")
public class CertificadoServiceImpl implements CertificadoService {

	@Autowired
	@Qualifier("certificadoRepository")
	private CertificadoRepository certificadoRepository;

	@Autowired
	@Qualifier("certificadoConverter")
	private CertificadoConverter certificadoConverter;

	@Override
	public CertificadoModel addCertificado(CertificadoModel certificadoModel) {
		certificadoModel.setEstado(1);
		Certificado certificado = certificadoRepository
				.save(certificadoConverter.convertCertificadoModel2Certificado(certificadoModel));
		return certificadoConverter.convertCertificado2CertificadoModel(certificado);
	}

	@Override
	public List<CertificadoModel> listAllCertificado() {
		List<Certificado> certificados = certificadoRepository.findAll();
		List<CertificadoModel> certificadoModel = new ArrayList<CertificadoModel>();
		for (Certificado certificado : certificados) {
			certificadoModel.add(certificadoConverter.convertCertificado2CertificadoModel(certificado));
		}
		return certificadoModel;
	}

	@Override
	public CertificadoModel removeCertificado(int idCertificado) {
		CertificadoModel certificadoModel = new CertificadoModel();
		certificadoModel = certificadoConverter.convertCertificado2CertificadoModel(findCertificadoById(idCertificado));
		certificadoModel.setEstado(2);
		certificadoRepository.save(certificadoConverter.convertCertificadoModel2Certificado(certificadoModel));
		return certificadoModel;
	}

	@Override
	public CertificadoModel findCertificadoByIdModel(int idCertificado) {
		CertificadoModel certificadoModel = new CertificadoModel();
		certificadoModel = certificadoConverter.convertCertificado2CertificadoModel(findCertificadoById(idCertificado));
		return certificadoModel;
	}

	@Override
	public Certificado findCertificadoById(int idCertificado) {
		Certificado cert = new Certificado(0, "", 0, "");
		Certificado certificado = certificadoRepository.findById(idCertificado).orElse(null);
		if (null != certificado) {
			return certificado;
		}
		return cert;
	}

}
