/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inmobiliaria.service.impl;

import com.inmobiliaria.component.CiudadConverter;
import com.inmobiliaria.entity.Ciudad;
import com.inmobiliaria.model.CiudadModel;
import com.inmobiliaria.repository.CiudadRepository;
import com.inmobiliaria.service.CiudadService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service("ciudadServiceImpl")
public class CiudadServiceImpl implements CiudadService {

    @Autowired
    @Qualifier("ciudadRepository")
    private CiudadRepository ciudadRepository;
    
    @Autowired
    @Qualifier("ciudadConverter")
    private CiudadConverter ciudadConverter;
    
    
    @Override
    public CiudadModel addCiudad(CiudadModel ciudadModel) {
       ciudadModel.setEstado(1);
       Ciudad ciudad= ciudadRepository.save(ciudadConverter.convertCiudadModel2Ciudad(ciudadModel));
       return ciudadConverter.convertCiudad2CiudadModel(ciudad);
    }

    @Override
    public List<CiudadModel> listAllCiudad() {
        List<Ciudad> ciudades= ciudadRepository.findAll();
        List<CiudadModel> ciudadModel= new ArrayList<CiudadModel>();
        for(Ciudad ciudad: ciudades){
            ciudadModel.add(ciudadConverter.convertCiudad2CiudadModel(ciudad));
            
        }
        return ciudadModel;
    }

    @Override
    public Ciudad findCiudadById(int idCiudad) {
        Ciudad ci= new Ciudad(0,"",2);
        Ciudad ciudad= ciudadRepository.findById(idCiudad).orElse(null);
        if(null != ciudad){
            return ciudad;
        }
        return ci;
    }   

    @Override
    public CiudadModel removeCiudad(int idCiudad) {
        CiudadModel ciudModel= new CiudadModel();
        ciudModel= ciudadConverter.convertCiudad2CiudadModel(findCiudadById(idCiudad));
        ciudModel.setEstado(2);
        ciudadRepository.save(ciudadConverter.convertCiudadModel2Ciudad(ciudModel));
        return ciudModel;
    }

    @Override
    public CiudadModel findCiudadByIdModel(int idCiudad) {
        CiudadModel ciudadModel= new CiudadModel();
        ciudadModel= ciudadConverter.convertCiudad2CiudadModel(findCiudadById(idCiudad));
        return ciudadModel;
    }
    
}
