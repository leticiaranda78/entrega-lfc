package com.inmobiliaria.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.inmobiliaria.component.AgenciaConverter;
import com.inmobiliaria.entity.Agencia;
import com.inmobiliaria.model.AgenciaModel;
import com.inmobiliaria.repository.AgenciaRespository;
import com.inmobiliaria.service.BusquedaAgenciaService;

@Service("busquedaAgenciaServiceImpl")
public class BusquedaAgenciaServiceImpl implements BusquedaAgenciaService {

	@Autowired
	@Qualifier("agenciaRepository")
	private AgenciaRespository agenciaRespository;

	@Autowired
	@Qualifier("agenciaConverter")
	private AgenciaConverter agenciaConverter;

	@Override
	public List<AgenciaModel> buscarPorNombre(String nombre) {
		List<Agencia> agenciaList = agenciaRespository.findByNombre(nombre);
		List<AgenciaModel> agenciaModel = new ArrayList<AgenciaModel>();
		for (Agencia agencia : agenciaList) {
			agenciaModel.add(agenciaConverter.convertAgencia2AgenciaModel(agencia));
		}
		return agenciaModel;
	}

}
