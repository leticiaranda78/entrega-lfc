package com.inmobiliaria.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.inmobiliaria.component.AsesorConverter;
import com.inmobiliaria.entity.Asesor;
import com.inmobiliaria.model.AsesorModel;
import com.inmobiliaria.repository.AsesorRespository;
import com.inmobiliaria.service.BusquedaService;

@Service("busquedaServiceImpl")
public class BusquedaServiceImpl implements BusquedaService {

	@Autowired
	@Qualifier("asesorRepository")
	private AsesorRespository asesorRepository;

	@Autowired
	@Qualifier("asesorConverter")
	private AsesorConverter asesorConverter;

	@Override
	public List<AsesorModel> buscarPorNroCedula(int nroCedula) {
		List<Asesor> asesorList = asesorRepository.findByNroCedula(nroCedula);
		List<AsesorModel> asesorModel = new ArrayList<AsesorModel>();
		for (Asesor asesor : asesorList) {
			asesorModel.add(asesorConverter.convertAsesor2AsesorModel(asesor));
		}
		return asesorModel;
	}

}
