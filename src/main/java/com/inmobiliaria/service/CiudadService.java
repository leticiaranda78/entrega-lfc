/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.inmobiliaria.service;

import com.inmobiliaria.entity.Ciudad;
import com.inmobiliaria.model.CiudadModel;
import java.util.List;


public interface CiudadService {
        
    public abstract CiudadModel addCiudad(CiudadModel ciudadModel);
    
    public abstract List<CiudadModel> listAllCiudad();
    
    public abstract Ciudad findCiudadById(int idCiudad);
    
    public abstract CiudadModel removeCiudad(int idCiudad);
    
    public abstract CiudadModel findCiudadByIdModel(int idCiudad);
}
