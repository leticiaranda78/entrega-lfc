package com.inmobiliaria.service;

import java.util.List;

import com.inmobiliaria.entity.Asesor;
import com.inmobiliaria.model.AsesorModel;

public interface AsesorService {
	
	public abstract AsesorModel addAsesor(AsesorModel asesorModel);
	
	public abstract List<AsesorModel> listAllAsesor();

	public abstract Asesor findAsesorById(int idAsesor);

	public abstract AsesorModel removeAsesor(int idAsesor);

	public abstract AsesorModel findAsesorByIdModel(int idAsesor);
}
