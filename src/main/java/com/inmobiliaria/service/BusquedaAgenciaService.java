package com.inmobiliaria.service;

import java.util.List;

import com.inmobiliaria.model.AgenciaModel;

public interface BusquedaAgenciaService {
	public abstract List<AgenciaModel> buscarPorNombre(String nombre);
}
