package com.inmobiliaria.service;

import com.inmobiliaria.model.InmuebleModel;
import java.util.List;

public interface BusquedaInmuebleService {
	public abstract List<InmuebleModel> buscarPorTitulo(String titulo);
}
