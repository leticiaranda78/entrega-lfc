package com.inmobiliaria.service;

import com.inmobiliaria.entity.Certificado;
import com.inmobiliaria.model.CertificadoModel;
import java.util.List;

public interface CertificadoService {

	public abstract CertificadoModel addCertificado(CertificadoModel certificadoModel);

	public abstract List<CertificadoModel> listAllCertificado();

	public abstract Certificado findCertificadoById(int idCertificado);

	public abstract CertificadoModel removeCertificado(int idCertificado);

	public abstract CertificadoModel findCertificadoByIdModel(int idCertificado);
}
