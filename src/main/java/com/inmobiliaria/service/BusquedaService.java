package com.inmobiliaria.service;

import java.util.List;

import com.inmobiliaria.model.AsesorModel;

public interface BusquedaService {
	public abstract List<AsesorModel> buscarPorNroCedula(int nroCedula);
}
