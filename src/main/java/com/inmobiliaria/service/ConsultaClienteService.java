package com.inmobiliaria.service;

import com.inmobiliaria.entity.ConsultaCliente;
import com.inmobiliaria.model.ConsultaClienteModel;
import java.util.List;

public interface ConsultaClienteService {

	public abstract ConsultaClienteModel addConsultaCliente(ConsultaClienteModel ccModel);

	public abstract List<ConsultaClienteModel> listAllConsultaCliente();

	public abstract ConsultaCliente findConsultaClienteById(int idConsulta);

	public abstract ConsultaClienteModel removeConsultaCliente(int idConsulta);

	public abstract ConsultaClienteModel findConsultaClienteByIdModel(int idConsulta);

}
