package com.inmobiliaria.service;

import java.util.List;

import com.inmobiliaria.entity.Agencia;
import com.inmobiliaria.model.AgenciaModel;

public interface AgenciaService {

	public abstract AgenciaModel addAgencia(AgenciaModel agenciaModel);

	public abstract List<AgenciaModel> listAllAgencia();

	public abstract Agencia findAgenciaById(int idAgencia);

	public abstract AgenciaModel removeAgencia(int idAgencia);

	public abstract AgenciaModel findAgenciaByIdModel(int idAgencia);

}
