
package com.inmobiliaria.utils;

public class ElementosPagina {
	private int numero;
	private boolean actua;

	public int getNumero() {
		return numero;
	}

	public boolean isActua() {
		return actua;
	}

	public ElementosPagina(int numero, boolean actua) {
		this.numero = numero;
		this.actua = actua;
	}

	public ElementosPagina() {
		super();
	}

}
