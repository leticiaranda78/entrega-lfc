package com.inmobiliaria.component;

import com.inmobiliaria.entity.ConsultaCliente;
import com.inmobiliaria.model.ConsultaClienteModel;
import org.springframework.stereotype.Component;

@Component("consultaclienteConverter")
public class ConsultaClienteConverter {

	public ConsultaCliente convertConsultaClienteModel2ConsultaCliente(ConsultaClienteModel ccModel) {
		ConsultaCliente cc = new ConsultaCliente();

		cc.setIdConsulta(ccModel.getIdConsulta());
		cc.setNombre(ccModel.getNombre());
		cc.setApellido(ccModel.getApellido());
		cc.setEstado(ccModel.getEstado());
		cc.setEmail(ccModel.getEmail());
		cc.setTelefono(ccModel.getTelefono());
		cc.setConsulta(ccModel.getConsulta());

		return cc;
	}

	public ConsultaClienteModel convertConsultaCliente2ConsultaClienteModel(ConsultaCliente cc) {
		ConsultaClienteModel ccModel = new ConsultaClienteModel();
		ccModel.setIdConsulta(cc.getIdConsulta());
		ccModel.setNombre(cc.getNombre());
		ccModel.setApellido(cc.getApellido());
		ccModel.setEstado(cc.getEstado());
		ccModel.setEmail(cc.getEmail());
		ccModel.setTelefono(cc.getTelefono());
		ccModel.setConsulta(cc.getConsulta());
		return ccModel;
	}

}
