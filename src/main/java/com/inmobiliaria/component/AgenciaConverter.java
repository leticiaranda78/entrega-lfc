package com.inmobiliaria.component;

import org.springframework.stereotype.Component;

import com.inmobiliaria.entity.Agencia;
import com.inmobiliaria.model.AgenciaModel;

@Component("agenciaConverter")
public class AgenciaConverter {

	public Agencia convertAgenciaModel2Agencia(AgenciaModel agenciaModel) {
		Agencia agencia = new Agencia();
		agencia.setIdAgencia(agenciaModel.getIdAgencia());
		agencia.setNombre(agenciaModel.getNombre());
		agencia.setDireccion(agenciaModel.getDireccion());
		agencia.setTelefono(agenciaModel.getTelefono());
		agencia.setDiasHorarios(agenciaModel.getDiasHorarios());
		agencia.setEstado(agenciaModel.getEstado());
		return agencia;
	}

	public AgenciaModel convertAgencia2AgenciaModel(Agencia agencia) {
		AgenciaModel agenciaModel = new AgenciaModel();
		agenciaModel.setIdAgencia(agencia.getIdAgencia());
		agenciaModel.setNombre(agencia.getNombre());
		agenciaModel.setDireccion(agencia.getDireccion());
		agenciaModel.setTelefono(agencia.getTelefono());
		agenciaModel.setDiasHorarios(agencia.getDiasHorarios());
		agenciaModel.setEstado(agencia.getEstado());
		return agenciaModel;
	}

}
