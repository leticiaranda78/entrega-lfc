package com.inmobiliaria.component;

import org.springframework.stereotype.Component;

import com.inmobiliaria.entity.Asesor;
import com.inmobiliaria.model.AsesorModel;

@Component("asesorConverter")
public class AsesorConverter {

	public Asesor convertAsesorModel2Asesor(AsesorModel asesorModel) {
		Asesor asesor = new Asesor();
		asesor.setIdAsesor(asesorModel.getIdAsesor());
		asesor.setNroCedula(asesorModel.getNroCedula());
		asesor.setNombre(asesorModel.getNombre());
		asesor.setApellido(asesorModel.getApellido());
		asesor.setFechaNacimiento(asesorModel.getFechaNacimiento());
		asesor.setEmail(asesorModel.getEmail());
		asesor.setDireccion(asesorModel.getDireccion());
		asesor.setEstado(asesorModel.getEstado());
		return asesor;
	}
	
	public AsesorModel convertAsesor2AsesorModel(Asesor asesor) {
		AsesorModel asesorModel = new AsesorModel();
		asesorModel.setIdAsesor(asesor.getIdAsesor());
		asesorModel.setNroCedula(asesor.getNroCedula());
		asesorModel.setNombre(asesor.getNombre());
		asesorModel.setApellido(asesor.getApellido());
		asesorModel.setFechaNacimiento(asesor.getFechaNacimiento());
		asesorModel.setEmail(asesor.getEmail());
		asesorModel.setDireccion(asesor.getDireccion());
		asesorModel.setEstado(asesor.getEstado());
		return asesorModel;
	}
	
}
