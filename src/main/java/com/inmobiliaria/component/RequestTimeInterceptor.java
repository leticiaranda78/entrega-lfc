package com.inmobiliaria.component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

//por cada petici'on que se realiza, entre en esta clase

@Component("requestTimeInterceptor")
public class RequestTimeInterceptor extends HandlerInterceptorAdapter {

	private static final Log LOG = LogFactory.getLog(RequestTimeInterceptor.class);

	/*
	 * se ejecuta antes de entrar en el metodo del controlador antes de que se
	 * ejecute el controlador, guardar el tiempo actual en milisegundos
	 */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		request.setAttribute("startTime", System.currentTimeMillis());
		return true;

	}

	/*
	 * despues de ejecutar y antes de mandar la vista al navegador
	 * 
	 */
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		long startTime = (long) request.getAttribute("startTime");
		LOG.info("Url to: '" + request.getRequestURL().toString() + "' in '" + (System.currentTimeMillis() - startTime) + "'ms");
	}

}
