package com.inmobiliaria.component;

import org.springframework.stereotype.Component;

import com.inmobiliaria.entity.Certificado;
import com.inmobiliaria.model.CertificadoModel;

@Component("certificadoConverter")
public class CertificadoConverter {
	public Certificado convertCertificadoModel2Certificado(CertificadoModel certificadoModel) {
		Certificado certificado = new Certificado();

		certificado.setIdCertificado(certificadoModel.getIdCertificado());
		certificado.setTitulo(certificadoModel.getTitulo());
		certificado.setEstado(certificadoModel.getEstado());
		certificado.setDescripcion(certificadoModel.getDescripcion());
		return certificado;
	}

	public CertificadoModel convertCertificado2CertificadoModel(Certificado certificado) {
		CertificadoModel certModel = new CertificadoModel();
		certModel.setIdCertificado(certificado.getIdCertificado());
		certModel.setTitulo(certificado.getTitulo());
		certModel.setEstado(certificado.getEstado());
		certModel.setDescripcion(certificado.getDescripcion());
		return certModel;
	}
}
