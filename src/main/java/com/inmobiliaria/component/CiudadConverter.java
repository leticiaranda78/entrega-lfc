
package com.inmobiliaria.component;

import com.inmobiliaria.entity.Ciudad;
import com.inmobiliaria.model.CiudadModel;
import org.springframework.stereotype.Component;

@Component("ciudadConverter")
public class CiudadConverter {

	public Ciudad convertCiudadModel2Ciudad(CiudadModel ciudadModel) {
		Ciudad ciudad = new Ciudad();
		ciudad.setIdCiudad(ciudadModel.getIdCiudad());
		ciudad.setDescripcion(ciudadModel.getDescripcion());
		ciudad.setEstado(ciudadModel.getEstado());
		return ciudad;
	}

	public CiudadModel convertCiudad2CiudadModel(Ciudad ciudad) {
		CiudadModel ciudadModel = new CiudadModel();
		ciudadModel.setIdCiudad(ciudad.getIdCiudad());
		ciudadModel.setDescripcion(ciudad.getDescripcion());
		ciudadModel.setEstado(ciudad.getEstado());
		return ciudadModel;
	}
}
